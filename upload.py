#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 10:34:56 2020

Script to create a record (DOI) in B2SHARE traning enviroment from a json doc with all metadata
and add a file(s) to this record. Finally publish the record.

Workflow:
1. Create draft
2. Upload files into draft record
2.(Update record metadata)
3. Submit for publication

@author: txellfe
"""
import sys
import requests
import json
from argparse import ArgumentParser
import logging
import os


def main(schema_json, submit_api_endpoints, eudat_token, file=None):
    
    # check whether submit_api_endpoints exists
    try:
        #endpoints_json_dir = os.path.dirname(os.path.abspath(submit_api_endpoints))
        with open(submit_api_endpoints, 'r') as f:
            config_params = json.load(f)
            
        #Collect data
        host = config_params["endpoint"]
        
        getResult, created = createDraftRecord(schema_json, host, eudat_token)
        if (created):
            bucketID = fileBucketID(getResult)
            DOI = recordID(getResult)
            logging.info("\n==Draft created:"+DOI)
            
            if (file is not None):
                #add files to record
                addFileToRecord(bucketID, file, host, eudat_token)
            
            #to publish
            published, full_DOI = updateMetadata(DOI, host, eudat_token)
            if(published):
                sys.stdout.write("\n==Record correcltly published with DOI: "+full_DOI)
            else:
                logging.fatal("Record NOT correcltly published")
                sys.exit(1)
          
                
        else:
            logging.fatal("Error creating DOI")
            sys.exit(1)
        
        
        
    except Exception as e:
        logging.fatal(e)
        sys.exit(1)
        
    
    
    

#Creates a draft reacord to B2SHARE 
def createDraftRecord (jsonMetadataFile, host, token):
  
    created = False
    try:
        with open(jsonMetadataFile) as json_file: 
            metadata = json.load(json_file) 
        
        
        header = {"Content-Type": "application/json"}
        url = host+"/records/"
        response = requests.request("POST", url, params={'access_token': token}, data=json.dumps(metadata),headers=header)
        #print(response.text.encode('utf8'))
        if (response.status_code == 201):
             created = True
             
        elif (response.status_code == 401):
             logging.fatal(response.json()['message'])
             sys.exit(1)
        return response, created
    
    except IOError or TypeError:
        logging.fatal(jsonMetadataFile+ " is missing or has incorrect format")
        sys.exit()

#Gets identifier for a set of files of draft record
def fileBucketID(responseUpload):
    
    result = json.loads(responseUpload.text)
    fileBucketID = result["links"]["files"]
    bucket = fileBucketID.rsplit('/', 1)[-1]
    return bucket

#Gets identifier of the draft record DOI
def recordID(responseUpload):
    
    result = json.loads(responseUpload.text)
    DOI = result["id"]

    return DOI


#Links a given file to the draft record
def addFileToRecord(fileBucketID, fileToAdd, host, token):
    filename = os.path.basename(fileToAdd)
    try: 
        url = host+"/files/"+fileBucketID+"/"+filename+"?access_token="+token
    
        payload = open(fileToAdd, "r")
        payload = payload.read()
        
        headers = {
          'Accept': 'application/json',
          'Content-Type': 'application/octet-stream'
        }
        
        response = requests.request("PUT", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        if (response.status_code == 200):
             logging.info("Added file with success")
        else:
            logging.fatal(response.json()['message'])
            sys.exit(1)
            
    except IOError:
        logging.fatal(fileToAdd+" is missing or has incorrect format")
        sys.exit(1)
        

    
    
#Update metadata record to published state
def updateMetadata(draftID, host, token):

    published = False
    full_DOI = None
    url = host+"/records/"+draftID+"/draft?access_token="+token
    
    #payload = "[\n    {\"op\": \"add\", \"path\":\"/publication_state\", \"value\": \"submitted\"},\n    { \"op\": \"add\", \"path\": \"/community_specific\", \"value\": {} }\n    \n    ]"
    payload = "[{\"op\": \"add\", \"path\":\"/publication_state\", \"value\": \"submitted\"}]"
    headers = {
      'Content-Type': 'application/json-patch+json'
    }
    response = requests.request("PATCH", url, headers=headers, data = payload)
    #saveToFile(path, response)
    if (response.status_code == 200):
         #print(response.text.encode('utf8'))
         published = True
         full_DOI =json.loads(response.text)['metadata']['DOI']
         
    else:
        logging.fatal(response.json()['message'])
        sys.exit(1)
    
    return published, full_DOI
    



if __name__ == "__main__":
    
    parser = ArgumentParser()
    parser.add_argument("-i", "--schema_json",
                        help="json file which contains all parameters for migration", required=True)
    parser.add_argument("-cr", "--submit_api_endpoints",
                        help="Endpoints for submission to EUDAT", required=True)
    parser.add_argument("-tk", "--eudat_token",
                        help="EUDAT token used for submission.", required=True)
    parser.add_argument("-f", "--file",
                        help="File to add to register")
    args = parser.parse_args()
    
    
    logging.basicConfig(level=logging.INFO)
    main(args.schema_json, args.submit_api_endpoints, args.eudat_token, args.file)
