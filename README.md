# WARNING: this project is archived.

The original work behind it is now performed by `oeb-participant-publish.py`, which is hosted at [OEB Scientific Admin Tools](https://gitlab.bsc.es/inb/elixir/openebench/oeb-sci-admin-tools) .

# B2SHARE

B2SHARE platform is a long-term repository. Files can be upload via GUI or via API. Each register has an assigned DOI, with metadata (compulsory to add) and attached files to it (optional). Data can be uploaded on different communities following the schema metadata attributes of the community. [B2SHARE](https://eudat.eu/services/userdoc/b2share#About )

## B2SHARE interface with OpenEBench

Application used to upload massively records comming from OpenEBench platform.
Two scripts are made: the first one (downloadRecord.py) to download records from B2SHARE when a DOI is given. It also downloads its attached files.
The second one (upload.py), it takes as input a json file with all metadata required (according to the JSON_schema of the community to upload). It can also take file(s) to attached to the future created record.

## Parameters
```
usage: upload.py [-h] -i SCHEMA_JSON -cr SUBMIT_API_ENDPOINTS -tk EUDAT_TOKEN
                 [-f FILE]

optional arguments:
  -h, --help            show this help message and exit
  -i SCHEMA_JSON, --schema_json SCHEMA_JSON
                        json file which contains all parameters for migration
  -cr SUBMIT_API_ENDPOINTS, --submit_api_endpoints SUBMIT_API_ENDPOINTS
                        Endpoints for submission to EUDAT
  -tk EUDAT_TOKEN, --eudat_token EUDAT_TOKEN
                        EUDAT token used for submission.
  -f FILE, --file FILE  File to add to register
```


## Installation
Requirements:

* Python interpreter

```bash
sudo apt update
sudo apt install python3.6 
```

* Metadata JSON schema of the community to upload data [Communities](https://b2share.eudat.eu/communities/)

## Usage
First, install the Python dependencies in a virtual environment:

```bash
python3 -m venv .py3env
source .py3env/bin/activate
pip install --upgrade pip wheel
pip install -r requirements.txt
```


To download a DOI:
```bash
python downloadRecord.py -d {DOI} -cr api_endpoints.json  -p path_dir_to_save
```

To upload a record with attached files:
```bash
python upload.py -i metadata.json -cr api_endpoints.json -tk {eudat_token} -f file.txt
```

Look for the community which you want to publish data and get their JSON_schema of metadata.
Fill the api_endpoints.json with the correct endpoint.



## To consider
A demo video was made on December 2020. This video shows the integration of this script to VRE. [demo](https://www.youtube.com/watch?v=YNUziurPBIc&t=1s)

