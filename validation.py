#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 13:57:43 2020

@author: txellfe
"""

import json
import jsonschema
from jsonschema import validate

    

def get_schema(filename):
    """This function loads the given schema available"""
    with open(filename, 'r') as file:
        schema = json.load(file)
    return schema


def validate_json(json_data, schemaFileName):
    """REF: https://json-schema.org/ """
    # Describe what kind of json you expect.
    execute_api_schema = get_schema(schemaFileName)

    try:
        validate(instance=json_data, schema=execute_api_schema)
    except jsonschema.exceptions.ValidationError as err:
        print(err)
        err = "Given JSON data is InValid"
        return False, err

    message = "Given JSON data is Valid"
    return True, message


