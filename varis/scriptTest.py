#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 19:24:07 2020
Script to get a specific DOI from B2SHARE (to execute by terminal)

@author: txellfe
"""
import sys
import requests

def main():
    
    if len(sys.argv) == 2:
        inputDOI = sys.argv[1]
        print(inputDOI)
        #Record ID to download
        #inputDOI = "484d7ea5436144698a494d503ad334a3"
        getResult = getSpecificDOI(inputDOI)
        print(getResult.text.encode('utf8'))
        #Save to file
        saveToFile("files/"+inputDOI+".json", getResult)
    
    else:
        print("Error - Not correctly introduce arguments")
        
    




def getSpecificDOI (DOI):
    """
    Gets a specific DOI and save the answer (json data) to a file

    Parameters
    ----------
    DOI : TYPE string
        DESCRIPTION. Id of the record to download

    Returns
    -------
    response : TYPE
        DESCRIPTION answer of the get request

    """
    payload = {}
    headers= {}
    url = "https://b2share.eudat.eu/api/records/"+DOI+"?access_token=KCSjfwWD6LjHi8zVgP2VYCQ4K3AGWFoAIGSubuKIFjbCQayfx11jumYINjTp"
    response = requests.request("GET", url, headers=headers, data = payload)
    
    return response
    


def getAllRecords():
    """
    Lists all records

    Returns
    -------
    TYPE request response class
        DESCRIPTION.

    """
    url = "https://b2share.eudat.eu/api/records?access_token=KCSjfwWD6LjHi8zVgP2VYCQ4K3AGWFoAIGSubuKIFjbCQayfx11jumYINjTp"
    payload = {}
    headers= {}

    return requests.request("GET", url, headers=headers, data = payload)
     
    



def saveToFile (filepath, data):
    """
    Save data to a given file

    Parameters
    ----------
    filepath : TYPE string
        DESCRIPTION: path wehre to save the file
    data : TYPE requests.models.Response
        DESCRIPTION: to save

    Returns
    -------
    None.

    """
    file = open(filepath, "w")
    file.write(data.text)
    file.close()
    
    


if __name__ == "__main__":
    main()

