#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 16:26:20 2020

@author: txellfe
"""
import requests
import json
import jsonpatch

token = "CcPEWY59ylwOFZCIAyI0wyHRlgldSR8dFtPzshbFKnC6A2L1umi7IFP3uO6v"
params = {'access_token': token}
recordid = 'fd022548c6494bf0b3ed61ec2f26aa26'

url = "https://trng-b2share.eudat.eu/api/records/" + recordid + "/draft"
r = requests.get(url, params=params)
result = json.loads(r.text)
#per saber id comunitat i aixi el seu schema
print(result["metadata"]["community"])

#nova metadata
metadata = {"publication_state":"submitted"}

metadata_old = result["metadata"]
print(json.dumps(metadata_old, indent=4))

patch = jsonpatch.make_patch(metadata_old, metadata)
print(patch)
finpatch = filter(lambda x: x["op"] != "remove", patch)
print(list(finpatch))

strpatch = json.dumps(list(finpatch))
print(strpatch)

headers = {'Content-Type': 'application/json-patch+json'}
r = requests.patch(url, data=strpatch, params=params, headers=headers)
result = json.loads(r.text)
print(json.dumps(result, indent=4))