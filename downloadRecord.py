#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 19:24:07 2020
Script to get a specific DOI from B2SHARE 
Also is getting its linked file(s) if it has

@author: txellfe
"""
import sys
import requests
import json
import wget
import hashlib
import os
from argparse import ArgumentParser
import logging

#Example: 1c48e5950ac94bdaa3fd725ce42ed9cb (4files)
#Example: d4661e6471bc42f58eab2c96b03804f7 (0files)

def main(register_doi, api_endpoints, save_path):
    # check whether api_endpoints exists
    try:
        with open(api_endpoints, 'r') as f:
            config_params = json.load(f)
            
        #Collect data
        host = config_params["endpoint"]
    
        r, done = getSpecificDOI(register_doi, host)
        #print(r.text.encode('utf8'))
        
        if (done):
            #Save to file
            if (os.path.isdir(save_path)):
                if (saveToFile(save_path+"/"+register_doi+".json", r)):
                    logging.info("Metadata correctly saved")
        
                #get files
                b = getBucketID(r)
                if (b != False):
                    arr, check = getFiles(b, host)
                    downloadFiles(arr, check, save_path)
                    
                else:
                    logging.info("No files to download")
            else: 
                 logging.fatal("Path directory does not exists.")
                 sys.exit(1)
        else:
            logging.fatal("Introduced DOI "+register_doi+" not found.")
            sys.exit(1)
            
    
    except Exception as e:
        logging.fatal(e)
        sys.exit(1)
    



#Gets a specific DOI and save the answer (json data) to a file
def getSpecificDOI (DOI, host):
    
    executed = False
    payload = {}
    headers= {}
    url = host+"/records/"+DOI
    response = requests.request("GET", url, headers=headers, data = payload)
    if (response.status_code == 200):
        executed = True
    
    return response, executed
    

def getBucketID (response):
    result = json.loads(response.text)
    #if there are files attached
    if (len(result["files"]) != 0):
        fileBucketID = result["links"]["files"]
        bucket = fileBucketID.rsplit('/', 1)[-1]
        return bucket
    
    #no files attached
    else:
        return False


def getFiles (bucket, host):
    url = host+"/files/"+bucket

    payload = {}
    headers= {}
    
    response = requests.request("GET", url, headers=headers, data = payload)
    
    #print(response.text.encode('utf8'))
    
    result = json.loads(response.text)
    
    #get all links to file to download into array
    #get md5 of that files
    listLinksFiles = []
    checksumFiles = []
    for i in range(len(result["contents"])):
        listLinksFiles.append(result["contents"][i]["links"]["self"])
    for i in range(len(result["contents"])):
        checksumFiles.append((result["contents"][i]["checksum"]).rsplit(':',1)[-1])

    return listLinksFiles, checksumFiles

def downloadFiles (arrayLinks, checksumFiles, pathToDownload):
    
    try: 
        #Download files from the links into the array
        for i in range(len(arrayLinks)):
            wget.download(arrayLinks[i], pathToDownload)
            
            if(checkCorruption(pathToDownload+"/"+(arrayLinks[i].rsplit('/', 1)[-1]),checksumFiles[i] )):
                print("Valid file. Successfully downloaded "+arrayLinks[i].rsplit('/', 1)[-1])
            else: 
                #try 3 times to download that file
                for j in range(3):
                    wget.download(arrayLinks[i], pathToDownload)
                    if(checkCorruption(pathToDownload+"/"+(arrayLinks[i].rsplit('/', 1)[-1]),checksumFiles[i] )):
                        logging.info("Valid file. Successfully downloaded "+arrayLinks[i].rsplit('/', 1)[-1])
                        break;
                    else: 
                        logging.info("Unuccessfully downloaded "+i.rsplit('/', 1)[-1])
  
        logging.info("Downloaded "+ str(len(arrayLinks)) + " files. Correctly saved.")
        
    except IOError:
        logging.fatal("Error to download file(s)")
        

def saveToFile (filepath, data):
    """
    Save data to a given file

    Parameters
    ----------
    filepath : TYPE string
        DESCRIPTION: path wehre to save the file
    data : TYPE requests.models.Response
        DESCRIPTION: to save

    Returns
    -------
    None.

    """
    try: 
        file = open(filepath, "w")
        file.write(data.text)
        file.close()
        return True
    
    except IOError:
        logging.fatal("Error to save  metadata file")
        sys.exit(1)
        
#checks if a file is corrupted    
def checkCorruption(filename, original_md5):
    try: 
        with open(filename,"rb") as f:
            bytes = f.read() # read file as bytes
            readable_hash = hashlib.md5(bytes).hexdigest();
            print("\n")
            logging.info("Hash check: "+readable_hash)
        
        # Finally compare original MD5 with freshly calculated
        if original_md5 == readable_hash:
            return True
        else:
            return False
    except IOError:
         logging.info("Not possible to check file")
    
    
    
if __name__ == "__main__":
    
    parser = ArgumentParser()
    parser.add_argument("-d", "--register_doi",
                        help="DOI of the register to download", required=True)
    parser.add_argument("-cr", "--api_endpoints",
                        help="Endpoints for submission to EUDAT", required=True)
    parser.add_argument("-p", "--save_path",
                        help="Path to save the download register.", required=True)
    args = parser.parse_args()
    
    
    logging.basicConfig(level=logging.INFO)
    main(args.register_doi, args.api_endpoints, args.save_path)


